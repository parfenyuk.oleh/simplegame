'use strict';

let theParent = document.querySelector("#container"),
	blocks = [...theParent.children],
	gameStarted = false,
	disableStartButton = false,
	clickWasMade = false,
	filledBlock = null,
	delay = null,
	gameInterval = null,
	disableFillTimeout = null,
	winButton = document.querySelector("#winButton"),
	messageBlock = document.getElementById("message"),
	startButton = document.querySelector("#startButton");

const dialog = {
	wrongInput: 'Wrong value',
	playerWon: 'Congratulations, you are win!',
	pcWon: 'You lose:'
}

theParent.addEventListener("click", clickEvent, false);
startButton.addEventListener("click", startBattle, false);

function closeWindow() {
	location.reload();
}

function clickEvent(e) {

	if (e.target !== e.currentTarget && gameStarted) {
		let index = null;
		clickWasMade = true;
		if (e.target.classList.contains('active')) {
			index = blocks.indexOf(e.target);

			hitBlockFill(index);
		}
		else {
			index = blocks.indexOf(filledBlock)

			missBlockFill(index)
		}
	}
	e.stopPropagation();
}

function openMessage(event, score) {
	let message = '';
	switch (event) {
		case "wrongInput":
			message = dialog.wrongInput;
			setTimeout(function () {
				messageBlock.classList.add('hidden');
				messageBlock.innerHTML = '';
			}, 2000);
			break;
		case "playerWon":
			message = dialog.playerWon;
			break;
		case "pcWon":
			message = dialog.pcWon;
			break;
	}

	if (score) {
		clearInterval(gameInterval);
		winButton.classList.remove('hidden');
		message = message + '<br/>' + ' Your score: ' + score.user + '<br/>' + 'PC score: ' + score.pc;
	}
	messageBlock.classList.remove('hidden');
	messageBlock.innerHTML = message + messageBlock.innerHTML;

}

function hitBlockFill(itemIdx) {
	blocks[itemIdx].classList.add("hit");
	addPoints('user');
}

function missBlockFill(itemIdx) {
	blocks[itemIdx].classList.add("miss");
	setTimeout(function () {
		blocks[itemIdx].className = 'block';
	}, 500);
	addPoints();
}

function generateRandomFill(delay) {
	let randBlock = Math.floor((Math.random() * 100));
	filledBlock = blocks[randBlock];
	filledBlock.classList.add("active");
	disableStartButton = true;
	disableFillTimeout = setTimeout(function () {
		blocks[randBlock].className = 'block';
		if (clickWasMade) {
			clickWasMade = false;
		}
		else {
			missBlockFill(randBlock)
		}
		disableStartButton = false;
	}, delay);
}

function updateScore(elem) {
	let score = null;
	elem.innerText = parseInt(elem.innerText) + 1;
	if (parseInt(elem.innerText) === 10) {
		score = {
			user: document.getElementById("user").innerHTML,
			pc: document.getElementById("pc").innerHTML
		}
		theParent.removeEventListener("click", clickEvent, false);
		switch (elem.id) {
			case 'pc':
				openMessage('pcWon', score);
				break;
			case 'user':
				openMessage('playerWon', score);
				break;
		}
		return;
	}
}

function startBattle() {
	if(!gameStarted){
		delay = parseInt(document.getElementById('delay').value);

		if (!disableStartButton && !isNaN(delay) && delay > 0) {
			gameStarted = true;
			gameInterval = setInterval(function () {
				generateRandomFill(delay);
			}, delay);
		}
		else {
			openMessage('wrongInput');
		}
	}
}

function addPoints(opponent) {
	switch (opponent) {
		case 'user':
			var elem = document.getElementById("user");
			updateScore(elem)
			break;
		default:
			var elem = document.getElementById("pc");
			updateScore(elem)
	}
}

